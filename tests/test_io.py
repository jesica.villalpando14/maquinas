from nose.tools import *
import tempfile
import os

from maquinas.io import *

dfa="""
         |  a  | b  |
-> q0    | q0 | q1 |
   q1  ] | q1 | q0 |
"""

ndfa="""
         |  a    | b  |
-> q0    | q0    | q1 |
   q1  ] | q1    |    |
   q2    | q1,q2 | q1,q2  |
"""

ndfa_e="""
         |  a    | b  | epsilon 
-> q0    | q0    |    | q2
   q1  ] | q1    | q0 | 
   q2    | q1,q2 | q1,q2  | q2
"""

jflap_dfa="""
<structure>
<type>fa</type>
<automaton>
<!--The list of states.-->
<state id="0" name="q0">
<x>83.0</x>
<y>139.0</y>
<initial/>
</state>
<state id="1" name="q1">
<x>235.0</x>
<y>61.0</y>
</state>
<state id="2" name="q2">
<x>545.0</x>
<y>136.0</y>
</state>
<state id="3" name="q3">
<x>391.0</x>
<y>231.0</y>
</state>
<state id="4" name="q4">
<x>287.0</x>
<y>285.0</y>
</state>
<state id="5" name="q5">
<x>504.0</x>
<y>391.0</y>
</state>
<state id="6" name="q6">
<x>161.0</x>
<y>329.0</y>
<final/>
</state>
<!--The list of transitions.-->
<transition>
<from>3</from>
<to>5</to>
<read>a</read>
</transition>
<transition>
<from>1</from>
<to>2</to>
<read>b</read>
</transition>
<transition>
<from>4</from>
<to>5</to>
<read>b</read>
</transition>
<transition>
<from>5</from>
<to>4</to>
<read>b</read>
</transition>
<transition>
<from>3</from>
<to>4</to>
<read>b</read>
</transition>
<transition>
<from>6</from>
<to>1</to>
<read>a</read>
</transition>
<transition>
<from>4</from>
<to>6</to>
<read>a</read>
</transition>
<transition>
<from>1</from>
<to>0</to>
<read>a</read>
</transition>
<transition>
<from>0</from>
<to>2</to>
<read>b</read>
</transition>
<transition>
<from>2</from>
<to>0</to>
<read>b</read>
</transition>
<transition>
<from>6</from>
<to>5</to>
<read>b</read>
</transition>
<transition>
<from>0</from>
<to>6</to>
<read>a</read>
</transition>
<transition>
<from>5</from>
<to>2</to>
<read>a</read>
</transition>
<transition>
<from>2</from>
<to>3</to>
<read>a</read>
</transition>
</automaton>
</structure>
"""


jflap_nfa="""
<structure>
<type>fa</type>
<!--The list of states.-->
<state id="9">
<x>317.0</x>
<y>284.0</y>
</state>
<state id="7">
<x>467.0</x>
<y>62.0</y>
</state>
<state id="8">
<x>585.0</x>
<y>61.0</y>
</state>
<state id="0">
<x>68.0</x>
<y>215.0</y>
<initial/>
</state>
<state id="4">
<x>242.0</x>
<y>59.0</y>
</state>
<state id="5">
<x>373.0</x>
<y>56.0</y>
</state>
<state id="2">
<x>237.0</x>
<y>164.0</y>
</state>
<state id="10">
<x>209.0</x>
<y>345.0</y>
</state>
<state id="3">
<x>311.0</x>
<y>117.0</y>
</state>
<state id="12">
<x>462.0</x>
<y>354.0</y>
</state>
<state id="1">
<x>169.0</x>
<y>214.0</y>
</state>
<state id="6">
<x>524.0</x>
<y>125.0</y>
<final/>
</state>
<state id="11">
<x>564.0</x>
<y>292.0</y>
<final/>
</state>
<!--The list of transitions.-->
<transition>
<from>3</from>
<to>6</to>
<read/>
</transition>
<transition>
<from>2</from>
<to>3</to>
<read>a</read>
</transition>
<transition>
<from>1</from>
<to>9</to>
<read>a</read>
</transition>
<transition>
<from>9</from>
<to>11</to>
<read/>
</transition>
<transition>
<from>6</from>
<to>7</to>
<read>b</read>
</transition>
<transition>
<from>12</from>
<to>11</to>
<read>b</read>
</transition>
<transition>
<from>11</from>
<to>12</to>
<read>b</read>
</transition>
<transition>
<from>9</from>
<to>6</to>
<read/>
</transition>
<transition>
<from>3</from>
<to>11</to>
<read/>
</transition>
<transition>
<from>4</from>
<to>5</to>
<read>a</read>
</transition>
<transition>
<from>1</from>
<to>2</to>
<read>a</read>
</transition>
<transition>
<from>3</from>
<to>4</to>
<read>a</read>
</transition>
<transition>
<from>7</from>
<to>8</to>
<read>b</read>
</transition>
<transition>
<from>10</from>
<to>9</to>
<read>a</read>
</transition>
<transition>
<from>9</from>
<to>10</to>
<read>a</read>
</transition>
<transition>
<from>8</from>
<to>6</to>
<read>b</read>
</transition>
<transition>
<from>5</from>
<to>3</to>
<read>a</read>
</transition>
<transition>
<from>0</from>
<to>1</to>
<read>a</read>
</transition>
</structure>
"""

def test_load_dfa():
    """Test  load a DFA from string"""
    m=load_fa(dfa)
    assert len(m.Q)==2
    assert len(m.sigma)==2
    assert m.q_0=='q0'
    assert len(m.A)==1
    assert len(m.ttable)==2

def test_load_ndfa():
    """Test  load a NDFA from string"""
    m=load_fa(ndfa)
    assert len(m.Q)==3
    assert len(m.sigma)==2
    assert m.q_0=='q0'
    assert len(m.A)==1
    assert len(m.ttable)==3

def test_load_ndfa_e():
    """Test  load a NDFA-e from string"""
    m=load_fa(ndfa_e)
    assert len(m.Q)==3
    assert len(m.sigma)==3
    assert m.q_0=='q0'
    assert len(m.A)==1
    assert len(m.ttable)==3

def test_savefile():
    """Test saving file"""
    m=load_fa(dfa)
    outfile_path = tempfile.mkstemp()[1]
    m.save_file(outfile_path)
    assert os.path.exists(outfile_path)

def test_load_jflap_dfa():
    """Test loading jflap dfa"""
    m=load_jflap(jflap_dfa)
    assert len(m.Q)==7
    assert len(m.sigma)==2
    assert m.q_0=='q0'
    assert len(m.A)==1
    assert len(m.ttable)==7

def test_load_jflap_nfa():
    """Test loading jflap NDFA-e"""
    m=load_jflap(jflap_nfa, lambda x: f'q{x}')
    assert len(m.Q)==13
    assert len(m.symbols())==2
    assert m.q_0=='q0'
    assert len(m.A)==2
    assert len(m.ttable)==13

def test_string_dfa():
    """Creating string from DFA"""
    m1=load_fa(dfa)
    s=m1.to_string()
    m2=load_fa(s)
    assert len(m1.Q)==len(m2.Q)
    assert len(m1.symbols())==len(m2.symbols())
    assert m1.q_0==m2.q_0
    assert len(m1.A)==len(m2.A)
    assert len(m1.ttable)==len(m2.ttable)

def test_string_ndfa():
    """Creating string from NDFA"""
    m1=load_fa(ndfa)
    s=m1.to_string()
    m2=load_fa(s)
    assert len(m1.Q)==len(m2.Q)
    assert len(m1.symbols())==len(m2.symbols())
    assert m1.q_0==m2.q_0
    assert len(m1.A)==len(m2.A)
    assert len(m1.ttable)==len(m2.ttable)

def test_string_ndfa_e():
    """Creating string from NDFA-e"""
    m1=load_fa(ndfa_e)
    s=m1.to_string()
    m2=load_fa(s)
    assert len(m1.Q)==len(m2.Q)
    assert len(m1.symbols())==len(m2.symbols())
    assert m1.q_0==m2.q_0
    assert len(m1.A)==len(m2.A)
    assert len(m1.ttable)==len(m2.ttable)
