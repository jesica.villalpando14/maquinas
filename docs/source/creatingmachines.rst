Creating machines
=================

.. toctree::
   :maxdepth: 2

.. module:: maquinas

Regular machines
----------------

Deterministic Finete Automaton (DFA)
************************************

Create a :class:`~maquinas.regular.dfa.DeterministicFiniteAutomaton` from scratch:

.. code-block:: python

    from maquinas.regular.dfa import DeterministicFiniteAutomaton as DFA

    m=DFA(Q=['q_0','q_1'],
                         sigma=['a','b'],
                         q_0='q_0',
                         A=['q_0'],
                         delta=[
                            (('q_0','a'),'q_0'),
                            (('q_1','b'),'q_0'),
                            (('q_1','a'),'q_1'),
                            (('q_0','b'),'q_1'),
                         ]) 
    m.print_summary()

Non-Deterministic Finete Automaton (NDFA)
*****************************************

Create a :class:`~maquinas.regular.ndfa.NonDeterministicFiniteAutomaton`  from scratch:

.. code-block:: python

    from maquinas.regular.ndfa import NonDeterministicFiniteAutomaton as NDFA

    m=NDFA(Q=['q_0','q_1'],
                         sigma=['a','b'],
                         q_0='q_0',
                         A=['q_0'],
                         delta=[
                            (('q_0','a'),['q_0']),
                            (('q_1','b'),['q_0']),
                            (('q_1','a'),['q_1']),
                            (('q_0','b'),['q_1']),
                         ])
    m.print_summary()


Non-Deterministic Finete Automaton with ε-moves (NDFA-ε)
********************************************************

Create a :class:`~maquinas.regular.ndfa_e.NonDeterministicFiniteAutomaton_epsilon`  from scratch:

.. code-block:: python

    from maquinas.regular.ndfa_e import NonDeterministicFiniteAutomaton_epsilon as NDFA_e

    m=NDFA_e(Q=['q_0','q_1'],
                         sigma=['a','b'],
                         q_0='q_0',
                         A=['q_0'],
                         delta=[
                            (('q_0','ε'),['q_0']),
                            (('q_0','a'),['q_0']),
                            (('q_1','b'),['q_0']),
                            (('q_1','a'),['q_1']),
                            (('q_0','b'),['q_1']),
                         ])
    m.print_summary()


Context free machines
---------------------

Pushdown Automaton (PDA)
************************************

Create a :class:`~maquinas.contextfree.pda.PushDownAutomatoAutomaton`  from scratch:

.. code-block:: python

    from maquinas.contextfree.pda import PushDownAutomaton as PDA

    m=PDA(Q=['q_0','q_1','q_2'],
         sigma=['a','b'],
         gamma=['A'],
         q_0='q_0',
         A=['q_2'],
         delta=[
            (('q_0','a','Z0'),[('q_0','AZ0')]),
            (('q_0','a','A'),[('q_0','AA')]),
            (('q_0','b','A'),[('q_1','epsilon')]),
            (('q_1','b','A'),[('q_1','epsilon')]),
            (('q_1','epsilon','Z0'),[('q_2','Z0')]),
         ]
    )
    m.print_summary()


Recursively enumerable machines
-------------------------------

Two-Stack Pushdown Automaton (TSPDA)
************************************

Create a :class:`~maquinas.recursivelyenumerable.tspda.PushDownAutomatoAutomaton`  from scratch:

.. code-block:: python

    from maquinas.recursivelyenumerable.tspda import TwoStackPushDownAutomaton as PDA2

    m=PDA2(Q=['q_0','q_1','q_2','q_3'],
         sigma=['a','b'],
         gamma=['A'],
         q_0='q_0',
         A=['q_3'],
         delta=[
            (('q_0','a','Z0','Z0'),[('q_0','AZ0','AZ0')]),
            (('q_0','a','A','A'),[('q_0','AA','AA')]),
            (('q_0','b','A','A'),[('q_1','epsilon','A')]),
            (('q_1','b','A','A'),[('q_1','epsilon','A')]),
            (('q_1','c','Z0','A'),[('q_2','Z0','epsilon')]),
            (('q_2','c','Z0','A'),[('q_2','Z0','epsilon')]),
            (('q_2','epsilon','Z0','Z0'),[('q_3','Z0','Z0')]),
         ]
    )
    m.print_summary()

Turing Machine (TM)
*******************

Create a :class:`~maquinas.recursivelyenumerable.tm.TuringMachine`  from scratch:

.. code-block:: python

    from maquinas.recursivelyenumerable.tm import TuringMachine as TM

    m=TM(Q=['q_0','q_1','q_2','q_3','q_4'],
         sigma=['a','b'],
         gamma=['X','Y'],
         q_0='q_0',
         A=['q_4'],
         delta=[
            (('q_0','a'),[('q_1','X','R')]),
            (('q_0','Y'),[('q_3','Y','R')]),
            (('q_1','a'),[('q_1','a','R')]),
            (('q_1','b'),[('q_2','Y','L')]),
            (('q_1','Y'),[('q_1','Y','R')]),
            (('q_2','a'),[('q_2','a','L')]),
            (('q_2','Y'),[('q_2','Y','L')]),
            (('q_2','X'),[('q_0','X','R')]),
            (('q_3','Y'),[('q_3','Y','R')]),
            (('q_3','[B]'),[('q_4','[B]','R')]),
         ]
    )
    m.print_summary()
