Creating grammars
=================

.. toctree::
   :maxdepth: 2

.. module:: maquinas

Tokenization of rules
---------------------

For now the tokenization of rules is done only at the character level.


Regular grammars (RG)
---------------------

Create a  :class:`~maquinas.regular.rg.RegularGrammar` from a string:

.. code-block:: python

    from maquinas.regular.rg import RegularGrammar as RG

    g=aes_b_ces=RG('S → aS; S → bA; A → ε; A → cA') #a*bc*
    g.print_summary()



Context free grammars (CFG)
---------------------------

Create a :class:`~maquinas.contextfree.cfg.ContextFreeGrammar` from a string:

.. code-block:: python

    from maquinas.contextfree.cfg import ContextFreeGrammar as CFG

    g=CFG("S-> ACB; C-> ACB; C -> AB; A -> a; B->b")
    g.print_summary()

