Installation
============

Dependencies
------------

These dependencies will be installed automatically:

* Matplotlib
* graphviz
* IPython
* ordered_set
* Pillow
* TatSu

Create virtual enviroment
-------------------------

To better manage your code create a virtual environment  

.. code-block:: text

    $ mkdir project_mame
    $ cd project_name
    $ python3 -m venv venv


Activate enviroment
-------------------

Before working on your machines or grammars activate the corresponding enviroment

.. code-block:: text

    $ . venv/bin/activate

Install maquinas
----------------

Within the activated environment use pip to install maquinas:

.. code-block:: sh

    $ pip install maquinas

For examples of its use, check out the :doc:`/quickstart`.
